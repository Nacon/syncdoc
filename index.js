var express = require('express');
var util = require('util');
var url = require('url')
var http = require('http');
var net = require('net');
var bodyParser = require('body-parser')
var app = express();
    app.use(express.static('static'));
    app.use(bodyParser.json()); 
    app.use(bodyParser.urlencoded( { 
      extended: true
    })); 
    var server = http.createServer(app);
    var tcpServer;
    // const client = net.Socket();
    // ({port: 8080}, () => {
    //   //'connect' listener
    //   console.log('connected to server!');
    //   client.write('world!\r\n');
    // });

    // var tcpServer = net.createServer(function(stream) {
    //     stream.write("Created Server");
    // });

var fs = require('fs');
var ShareDB = require('sharedb');
var WebSocket = require('ws');
var WebsocketJSONStream = require('websocket-json-stream');
//Här anger vi en databas som används till shareDB. med MongoDB.
const db = require('sharedb-mongo')('mongodb://localhost:27017/test');

const backend = new ShareDB({ db });
var connection;
var obj = JSON.parse(fs.readFileSync('form1.json', 'utf8'));

var port = 8080;


app.set('view engine', 'html');
app.set('views', 'views');

app.engine('html', require('ejs').renderFile);

// app.get("/", function(req, res, next) {
//     res.render('home', {
//         showTitle: true,
//         helpers: {
//             foo: function () { return 'foo.'; }
//         }

//     });
//     console.log(req);
// });
// 

app.get('/login', function (req, res, next) {
    res.render('login.html' , {
    });
});

app.get('/', function (req, res, next) {

    res.render('patient.html' , {
    });
});

// app.post('/', function(req, res, next) {
//     console.log(obj);
//     res.send(obj);

// });


// app.post('/', function (req, res, next) {

//     if(req.body.type == "login") {

//         for (element in usersMap) {
//             if(element == req.body.username && usersMap[element] == req.body.pass) {
//                 connectedUsers[element] = req.body.pass;
//                 console.log("User " + element + " connected");
//                 res.redirect('/patient');
//             }
//         } 
//     }

//     else if (req.body.type == "createuser") {
//         checkUser(req, res);
//     }
// });

initiatePatients(startServer);

function initiatePatients(callback) {

    connection = backend.connect(); 

    //Create history log
    fs.readFile(("./log/historylog.txt"), function (err) {
        if(err) {
            fs.writeFile("./log/historylog.txt", "", function() {
                console.log("Log file was created");
            });
        }
        else {
            console.log("Log file exists, did not create file again");
        }
    });

    // var oneDoc = connection.get('patients', 'oneDoc');

    // oneDoc.fetch(function (err) {
    //     //console.log("Fetching doc!");
    //     if (err) throw err;
    //     if (oneDoc.type === null) {   
    //         oneDoc.create(obj);
    //         return;
    //     }
    // });

    callback();
}

function startServer() {
    //RED.stop();
    
    var wss = new WebSocket.Server({ server: server });

    wss.on('connection', function (ws) {
        ///console.log('user connected!')
        var stream = new WebsocketJSONStream(ws);

        //userCatcher(backend, users);

        ws.on('message', function (message) 
        {
            historyLogging(message);
        });

        ws.on('close', function close() {
            //console.log('A user disconnected');
        });
       
        backend.listen(stream);
    });

    tcpServer = net.createServer((c) => {
        // 'connection' listener
        console.log('client connected');

        c.on('end', () => {
            console.log('client disconnected');
        });

        c.on('data', function (data) {
            var obj = JSON.parse(data);

            //console.log(text);
            
            var doc = connection.get(obj.collection, obj.patient);

            doc.fetch(function(err) {
                if (err) throw err;
                if (doc.type != null) {  
                    console.log(util.inspect(obj));
                    if(obj.sender == "puls"){
                        
                        doc.submitOp({p:['formfields', 'pulsrate' ,'value'], od:doc.data.formfields.pulsrate.value, oi:obj.value});
                        doc.submitOp({p:['formfields', 'pulsrate' ,'colorClass'], od:doc.data.formfields.pulsrate.colorClass, oi:obj.colorClass});
                        doc.submitOp({p:['nodeSender'], od:doc.data.nodeSender.value, oi:obj.sender});
                    }

                    else if (obj.sender == "oxygen") {
                        doc.submitOp({p:['formfields', 'oxygen' ,'value'], od:doc.data.formfields.oxygen.value, oi:obj.value});
                        doc.submitOp({p:['formfields', 'oxygen' ,'colorClass'], od:doc.data.formfields.oxygen.colorClass, oi:obj.colorClass});
                        doc.submitOp({p:['nodeSender'], od:doc.data.nodeSender.value, oi:obj.sender});
                    }

                    else {
                        console.log("No sender... wait what?");
                    }
                }
            });

            // console.log(typeof obj);
            // console.log(util.inspect(obj.value));
        });
        c.pipe(c);
    });

    // client.connect(1337, '127.0.0.1', function() {
    //     console.log('Connected');
    //     client.write('Hello, server! Love, Client.');
    // });

    // client.on('data', function(data) {
    //     console.log('Received: ' + data);
    //     client.destroy(); // kill client after server's response
    // });

    // client.on('close', function() {
    //     console.log('Connection closed');
    // });

    

    // var settings = {

    //     httpAdminRoot:"/red",
    //     httpNodeRoot: "/api",
    //     functionGlobalContext: {
    //         conn:connection,
    //         foo: "bar"
    //     }
    // };

    // RED.init(server,settings);

    // // Serve the editor UI from /red
    // app.use(settings.httpAdminRoot,RED.httpAdmin);

    // // Serve the http nodes UI from /api
    // app.use(settings.httpNodeRoot,RED.httpNode);
    tcpServer.listen(1337, function() {
        console.log("Listening to TCP server on " + "1337");
    });
    
    server.listen(port, function() {
        //console.log('Listening on http://localhost:' + port);
    });
    
    //RED.start();

}

function historyLogging (message) {

    var messageJSONParsed = JSON.parse(message);
    var collectionKey = messageJSONParsed['c'];
    var operationKey = messageJSONParsed['op'];
    var idKey = messageJSONParsed['d'];
    // console.log("MESSAGE: ");
    console.log("MESSAGE!!!");
    console.log(util.inspect(messageJSONParsed));
    console.log("!!!MESSAGE");

    var time = new Date().getTime();
    var date = new Date(time);

    //Write history to log
    if (operationKey !== undefined) 
    {
        if (messageJSONParsed['c'] == 'usernames')
        {
            var username = messageJSONParsed['d'];
            //console.log(username);
            //fs.appendFile('./log/historylog.txt', " editor: " + username)
        }

        if (JSON.stringify(operationKey[0]['si']) && JSON.stringify(operationKey[1]) === undefined )
        {
            var addedValue = JSON.stringify(operationKey[0]['si']);
            fs.appendFile('./log/historylog.txt', "History: " + date.toString() + "  -  Added: " + addedValue + " , at field: " + idKey + " \n", function () {
                //
            });
        }

        else if (JSON.stringify(operationKey[0]['sd']) && JSON.stringify(operationKey[1]) === undefined ) 
        {
            var deletedValue = JSON.stringify(operationKey[0]['sd']);
            fs.appendFile('./log/historylog.txt', "History: " + date.toString() + "  -  Deleted: " + deletedValue + " , at field: " + idKey + " \n", function () {
                //
            });
        }

        else if (JSON.stringify(operationKey[0]['sd']) && JSON.stringify(operationKey[1]['si']))
        {
            var addedValue = JSON.stringify(operationKey[0]['sd']);
            var deletedValue = JSON.stringify(operationKey[1]['si']);
            fs.appendFile('./log/historylog.txt', "History: " + date.toString() + "  -  Replaced: " + addedValue + " with "  + deletedValue + " , at field: " + idKey + " \n", function () {
                // 
            });
        }
    }
}