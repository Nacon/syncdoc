﻿$(document).ready(function () {
    
    var sharedb = require('sharedb/lib/client');
    var StringBinding = require('sharedb-string-binding');
    var socket = new WebSocket('ws://' + 'localhost:8080/');
    var connection = new sharedb.Connection(socket);
    var elementId;
    var patientID;

    //Det dokumentet som ska skrivas eller visas för klienten

    function subscribeToPatientFields (patientID) {
//doc.destroy();
        doc = connection.get('patients', patientID);

        doc.subscribe(function (err) {
            if (err) throw err;
            for (var key in doc.data.formfields)
            {
                subscribeToAllFields(document.querySelector('#' + key), doc);
            }
            // setDocValues(doc);
            doc.on('op', function (source) {
                console.log("Operation!");
                operationChanged(doc.data);

            });
        });


    };
        

    function subscribeToPatientFields2(patientID) {
        doc = connection.get('patients', patientID);

        doc.on('op', function (source) {
            console.log("Operation!");
            operationChanged('operationChanged');

        });

        if (doc.data != null) {
            doc.subscribe(function (err) {
                if (err) throw err;
                for (var key in doc.data.formfields) {
                    console.log(key);
                    subscribeToAllFields(document.querySelector('#' + key), doc);
                }
                // setDocValues(doc);
            });
        }
    };

    function operationChanged(state) {
        var evt = new CustomEvent('operationChanged', { detail: state });
        window.dispatchEvent(evt);
    }


    function getDoc() {        
        return doc;
    }

    //doc is the document that contains all the information about the html. doc.data contains the JSON tree.
    
    
    //Set initial value to each field in the document.
    function subscribeToAllFields (element, docitem) {
        elementId = element.id;
        //console.log("DOCITEM: ");
        //console.log(docitem);
        if((typeof element.value) == 'string' && (elementId !== "activeRadioButton")) {
            var binding = new StringBinding(element, docitem, ['formfields', elementId ,'value']);
            binding.setup();
        }
        else if(elementId === "activeRadioButton") {
            setradiobutton();
        }
        
        else if (elementId == "checklistButtonsId") {
            setChecklistButtons();
        }
    };

    //Send a radiobutton operation via submitOp to the database. After the operation, the value will be 

//stored in the database.
    $("input[name='inlineRadioOption']").change(function () {
        var value = $('input[name=inlineRadioOption]:checked', '#activeRadioButton').val();
        doc.submitOp([{p:['formfields', 'activeRadioButton' ,'value'], od:doc.data.formfields.activeRadioButton.value, oi:value}]);
    });

    //Send a checkBox operation via submitOp to the database. After the operation, the value will be 

//stored in the database.
    $("input[name='checklistOption']").change(function () {
        var elementChecked = $(this)[0];
        if(elementChecked.checked === true) {
            doc.submitOp([{p:['formfields', 'checklistButtonsId' ,'value', elementChecked.value], oi:true}]);
        }
        else if(elementChecked.checked === false) {
            doc.submitOp([{p:['formfields', 'checklistButtonsId' ,'value', elementChecked.value], oi:false}]);
        }
    });


    function operationChanged(state) {
        var evt = new CustomEvent('operationChanged', { detail: state });

        window.dispatchEvent(evt);
    }

    function removePatient(patientID) {
		console.log("Removing Patient!");
            var patientDoc = connection.get('patients', patientID);

            patientDoc.fetch(function (err){
                if (err) throw err;
                if(patientDoc.type != null) {
			console.log("Not null");
                	patientDoc.del(function () {
                    		console.log("deleted document");
                	});
                }
            });
        };


    function initCollection(patientID, text) {
                var obj = JSON.parse(text);
                var doc = connection.get('patients', patientID);
                if(doc.type === null) {
                    doc.create(obj);
                    return;
                }
    };

    function setREDPushValue() {
        var value = doc.data.formfields.pulsrate.value;
        $('#pulsrate').val(value);
    }


    //After an operation has been ran. This function will set the radiobutton to a new value according to database.
    function setradiobutton() {
        var value = doc.data.formfields.activeRadioButton.value;
        switch (value) {
            case "1":
                $('#inlineRadio1').prop("checked",true);
            break;
            case "2":
                $('#inlineRadio2').prop("checked",true);    
            break;
            case "3":
                $('#inlineRadio3').prop("checked",true);
            break;
        }
    };
    //After an operation has been ran. This function will set the checkboxes to a new value according to database.
    function setChecklistButtons () {
        var docChecklist = doc.data.formfields.checklistButtonsId.value;
        for(var key in docChecklist) {
            if(docChecklist[key] === true) {
                var element = document.querySelector("#" + key + "box");
                document.getElementById(element.id).checked = true;
            }
            else {
                var element = document.querySelector("#" + key + "box");
                document.getElementById(element.id).checked = false;
            }
        }   
    };
    global.subscribeToPatientFields = subscribeToPatientFields
    global.initCollection = initCollection;
    global.removePatient = removePatient;
    global.getDoc = getDoc;
});