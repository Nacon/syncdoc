$(document).ready(function () {
    
    var sharedb = require('sharedb/lib/client');
    var StringBinding = require('sharedb-string-binding');
    var socket = new WebSocket('ws://' + '10.252.240.142:8080/');
    var connection = new sharedb.Connection(socket);
    var elementId;
    var patientID;
    var doc;

    $('#paused-icon').show();
    $('#syncing-icon').hide();

   $('#paused-icon').click(function() {
   		$('#paused-icon').hide();
   		$('#syncing-icon').show();
   });

   $('#syncing-icon').click(function() {
   		$('#paused-icon').show();
   		$('#syncing-icon').hide();
   });
    //Det dokumentet som ska skrivas eller visas för klienten

    function subscribeToPatientFields (patientID) {
//doc.destroy();
        doc = connection.get('patients', patientID);

        doc.subscribe(function (err) {
            if (err) throw err;
            for (var key in doc.data.formfields)
            {
                subscribeToAllFields(document.querySelector('#' + key), doc);
            }
            // setDocValues(doc);
          	
            doc.on('op', function (source, err) {
	        	//if(err) throw err;
	        	//console.log("Operation!");
	            operationChanged(source);

            });
            
        });
    };

          
    window.addEventListener('operationChanged', function (e) {
		//alert('Event')
		setREDPushValue(e.detail[0].p[1]);

		
        //akutDoc = e.detail;
     	//if(source[0].oi != null) {
    	// 	if(source[0].p[1] == "pulsrate") {
    	// 		setREDPushValue("puls");
    	// 	}
    	// 	else if(source[0].p[1] == "oxygen") {
    	// 		setREDPushValue("oxygen");
    	// 	}
    	// }
    });

    function operationChanged(state) {
        var evt = new CustomEvent('operationChanged', { detail: state });
        window.dispatchEvent(evt);
    }

    function getDoc() {        
        return doc;
    }

    //Set initial value to each field in the document.
    function subscribeToAllFields (element, docitem) {


    	if(element != null) {
		    elementId = element.id;
		    //console.log("DOCITEM: ");
		    //console.log(docitem);
		    if((typeof element.value) == 'string' && (elementId !== "activeRadioButton")) {
		        var binding = new StringBinding(element, docitem, ['formfields', elementId ,'value']);
		        binding.setup();
		    }
		    else if(elementId === "activeRadioButton") {
		        setradiobutton();
		    }
		    
		    else if (elementId == "checklistButtonsId") {
		        setChecklistButtons();
		    }
    	}
    	else {
    		console.log("Element is null");
    	}

    };

    //Send a radiobutton operation via submitOp to the database. After the operation, the value will be 

//stored in the database.
    $("input[name='inlineRadioOption']").change(function () {
        var value = $('input[name=inlineRadioOption]:checked', '#activeRadioButton').val();
        doc.submitOp([{p:['formfields', 'activeRadioButton' ,'value'], od:doc.data.formfields.activeRadioButton.value, oi:value}]);
    });

    //Send a checkBox operation via submitOp to the database. After the operation, the value will be 

//stored in the database.
    $("input[name='checklistOption']").change(function () {
        var elementChecked = $(this)[0];
        if(elementChecked.checked === true) {
            doc.submitOp([{p:['formfields', 'checklistButtonsId' ,'value', elementChecked.value], oi:true}]);
        }
        else if(elementChecked.checked === false) {
            doc.submitOp([{p:['formfields', 'checklistButtonsId' ,'value', elementChecked.value], oi:false}]);
        }
    });

    function removePatient(patientID) {
		console.log("Removing Patient!");
        var doc = connection.get('patients', patientID);

        doc.fetch(function (err){
            if (err) throw err;
            if(doc.type != null) {
				console.log("Not null");
            	doc.del(function () {
                		console.log("deleted document");
            	});
	        }
	    });
    };


    function initCollection(patientID, text) {
        var obj = JSON.parse(text);
        var doc = connection.get('patients', patientID);
        if(doc.data == null) {
            doc.create(obj);
            return;
        }
    };

    function setREDPushValue(elementId) {
		if(doc.id != null) {
			//console.log(elementId);
			
			if(elementId == "pulsrate") {
				var value = doc.data.formfields.pulsrate.value;
		    	$('#pulsrate').val(value);
				$('#'+elementId).attr('class', doc.data.formfields.pulsrate.colorClass);
		    	//document.getElementById(elementId).className += doc.data.formfields.pulsrate.colorClass;
		    	console.log(doc.data.formfields.pulsrate.colorClass);
		    	// changeColorAtField('pulsrate', "puls");
			}
			else if(elementId == "oxygen"){
				var value = doc.data.formfields.oxygen.value;
		    	$('#oxygen').val(value);
		    	$('#'+elementId).addClass(doc.data.formfields.oxygen.colorClass);
		    	//document.getElementById(elementId).className += doc.data.formfields.oxygen.colorClass;
		    	//console.log(doc.data.formfields.oxygen.colorClass);
		    	// changeColorAtField('oxygen', "oxygen");
			}
		}
    }


    //After an operation has been ran. This function will set the radiobutton to a new value according to database.
    function setradiobutton() {
        var value = doc.data.formfields.activeRadioButton.value;
        switch (value) {
            case "1":
                $('#inlineRadio1').prop("checked",true);
            break;
            case "2":
                $('#inlineRadio2').prop("checked",true);    
            break;
            case "3":
                $('#inlineRadio3').prop("checked",true);
            break;
        }
    };
    //After an operation has been ran. This function will set the checkboxes to a new value according to database.
    function setChecklistButtons () {
        var docChecklist = doc.data.formfields.checklistButtonsId.value;
        for(var key in docChecklist) {
            if(docChecklist[key] === true) {
                var element = document.querySelector("#" + key + "box");
                document.getElementById(element.id).checked = true;
            }
            else {
                var element = document.querySelector("#" + key + "box");
                document.getElementById(element.id).checked = false;
            }
        }   
    };

    function changeColorAtField (elementID,sender) {
		// var color;
		// if(sender == "puls") {
		// 	color = doc.data.formfields.pulsrate.color;
		// }
		// else if(sender == "oxygen") {
		// 	color = doc.data.formfields.oxygen.color;
		// }
		// else {
		// 	color = 'red';
		// }

		// $('#'+elementID).css('border-color', color);
		// document.getElementById("MyElement").className += sender.class;
    }

    global.subscribeToPatientFields = subscribeToPatientFields
    global.initCollection = initCollection;
    global.removePatient = removePatient;
    global.getDoc = getDoc;
});